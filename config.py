# General settings
DEBUG = True
DEFAULT_ARCHIVE_NAME = "tweets"

# Display settings
ORDER_DESCENDING = False
CASE_SENSITIVE = False

# Fetching settings
TWEETS_PER_FETCH = 200
INCLUDE_RETWEETS = False

# FLask settings
PORT = 5000
HOST = 127.0.0.1
