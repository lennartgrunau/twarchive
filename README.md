# TwArchive

## Description

_to-do_

## Requirements

* Python 3.6+

## Installation

```bash
make install
```

Alternatively, simply install the pip requirements:
```bash
pip3 install -r requirements.txt [--user]
```

## Usage

### Create your Twitter archive

1. When you request your Twitter data, Twitter will provide you a zip-compressed archive. This archive contains a file called `tweet.js`. Extract it and put it into the main directory of TwArchive.
2. Run `add_archive.py`. It'll create a new `tweets.csv` file containing your tweets.
3. `fetch.py` is meant to run periodically, for example by using a cronjob. It will fetch the last 200 tweets (or less) and add them to the `tweets.csv` file. As a result, your Twitter archive will always be up-to-date.

For using `fetch.py`, you need an app for the Twitter Developer API which you can create [here](https://apps.twitter.com). The app needs read and write access (access to direct messages is not nessecary). Afterwards, you will have four keys which you need to put into a file called `keys.json`. Take a look at `keys.json.template` for an example (or simply edit the file and rename it).

### Run TwArchive

TwArchive is meant to run locally as a web application on port `5000`. Use `make start` or run `run_webapp.py` to launch the app.

## Configuration

_to-do_
