from flask import redirect, render_template, request, url_for
from fetch import fetch as fetch_tweets
from search import search
from webapp import app
from webapp.forms import QueryForm
import config


@app.route("/", methods=["GET", "POST"])
def home():
    form = QueryForm()
    if form.validate_on_submit():
        # check the optional query parameters
        case_sensitive = request.values.get("case_sensitive", config.CASE_SENSITIVE)
        order_descending = request.values.get("descending", config.ORDER_DESCENDING)
        query_type = request.values.get("rf")
        query = request.values.get("query")
        archive = request.values.get("archive_name", config.DEFAULT_ARCHIVE_NAME)

        if (case_sensitive is not None):
            case_sensitive = bool(case_sensitive)
        if (order_descending is not None):
            order_descending = bool(order_descending)

        if (archive == ""):
            # Using the default value itself doesn't work, as there's always a value submitted.
            # In most cases this value is therefore empty.
            archive = config.DEFAULT_ARCHIVE_NAME

        # Search for tweets
        result_set = search(
            archive=archive,
            text=query if query_type == "text" else None,
            date=query if query_type == "date" else None,
            source=query if query_type == "source" else None,
            case_sensitive=case_sensitive,
            descending=order_descending
        )

        return render_template(
            "results.html",
            results=result_set,
            query=query,
            type=query_type,
            title="Results",
            count=len(result_set)
        )
    else:
        return render_template("results.html", form=form, title="Search")
