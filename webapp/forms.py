from flask_wtf import FlaskForm
from wtforms import StringField, RadioField, SubmitField, BooleanField
from wtforms.validators import DataRequired

class QueryForm(FlaskForm):
    rf = RadioField("Type", choices=[("text", "Text"), ("source", "Source"), ("date", "Date")])
    query = StringField("Query", validators=[DataRequired()])
    case_sensitive = BooleanField("Case sensitive")
    descending = BooleanField("Reverse list (newest to oldest)")
    archive_name = StringField("Archive", render_kw={"placeholder": "The filename of the archive. Default if empty."})
    submit = SubmitField("Search")
