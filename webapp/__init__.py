from flask import Flask

app = Flask(__name__)
app.config["SECRET_KEY"] = "5669d9237210eabb2ea70a50243eebb4"

from webapp.routes import *
