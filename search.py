import csv
import getopt
import os
import pprint
import re
import sys
import time
import urllib.parse
import html
import config
from datetime import datetime, timedelta

CSV_FILE_NAME = __file__[:-len(os.path.basename(__file__))] + "%s.csv"
LINK_REGEX = re.compile(r"https?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")

def search(archive = config.DEFAULT_ARCHIVE_NAME, source = None, text = None, date = None, case_sensitive = config.CASE_SENSITIVE, descending = config.ORDER_DESCENDING):
    """ Searches for tweets matching the given criteria.
        :param archive: the archive containing the tweets to search for.
        :param source: the name of the used client (e.g. Twitter for iPhone).
        :param text: textual content of the tweet.
        :param date: ...
        :param case_sensitive: whether or not the search should be case sensitive. Default: False
        :returns: a list of matching tweets, where each tweet is a list:
        [id, datetime, source, content]. """
    tweet_file = csv.reader(open(CSV_FILE_NAME % archive.lower(), "r", encoding="utf-8"))
    result_set = []

    if not (source or text or date):
        exit(0)

    for tweet in tweet_file:
        _tweet = tweet[:]

        # skip header
        if tweet[0] == "id":
            continue

        _tweet[1] = pretty_date(tweet[1])
        _tweet.append(timestamp(tweet[1]))

        append = True
        if source:
            append = (tweet[2] == source)

        if date:
            append = (parseDate(tweet[1]) == date)

        if text and append:
            content = urllib.parse.unquote(LINK_REGEX.sub("", tweet[3]))
            content = html.unescape(content)

            if not case_sensitive:
                content = content.lower()
                text = text.lower()

            append = text in content

        if append:
            result_set.append(_tweet)

    return sorted(result_set, key=lambda tw: tw[4], reverse=descending)

def parseDate(date):
    """Converts a Twitter datetime string into the YYYY-mm-dd standard."""
    new_date = datetime.strptime(date, "%a %b %d %X %z %Y")
    result = str(new_date.strftime("%Y-%m-%d"))
    return result

def pretty_date(date):
    """ Prettifies a date given in the default Twitter API format.
        :param date: the date, as string
        :returns: the date, as string, but pretty """
    new_date = datetime.strptime(date, "%a %b %d %X %z %Y")
    new_date += timedelta(seconds=time.localtime().tm_gmtoff)
    return str(new_date.strftime("%b %d, %Y at %X"))

def timestamp(date):
    """Gets the timestamp of a Twitter API string
       :param date: The date as string
       :returns: The POSIX timestamp """
    date = datetime.strptime(date, "%a %b %d %X %z %Y")
    return date.timestamp()

def usage():
    print(f"""\nExample usage:

    python search.py -t "#lastFM"

Parameters:
    -t (--text) <query>         The search query.
    -s (--source) <client>      The client that was used to submit the tweet.
    -a (--archive) <filename>   The tweet archive. Default is {config.DEFAULT_ARCHIVE_NAME}.csv. Optional.
    -c (--case)                 Sets the search to be case-sensitive. Optional.
    -d (--descending)           Orders the tweets descending (newest first). Optional.
    -l (--limit) <number>       Limits the number of results. Optional.
""")
    exit(0)

# Shell execution
if __name__ == "__main__":
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], "a:s:t:l:cdh", [
            "archive=", "source=", "text=", "limit=", "case", "descending", "help"])
    except getopt.GetoptError as err:
        sys.exit(err)

    (source, text, archive, limit, case_sensitive, desc) = (None, None, config.DEFAULT_ARCHIVE_NAME, 0, config.CASE_SENSITIVE, config.ORDER_DESCENDING)

    for (opt, arg) in opts:
        if opt in ("-s", "--source"):
            source = arg
        elif opt in ("-t", "--text"):
            text = arg
        elif opt in ("-a", "--archive"):
            archive = arg
        elif opt in ("-c", "--case"):
            case_sensitive  = True
        elif opt in ("-d", "--descending"):
            desc = True
        elif opt in ("-l", "--limit"):
            try:
                limit = int(arg)
            except ValueError:
                print("[WARNING] Invalid limit.")
        elif opt in ("-h", "--help"):
            usage()

    if (len(opts) == 0):
        usage()

    result_set = search(archive=archive, source=source, text=text, case_sensitive=case_sensitive, descending=desc)
    pprint.pprint(result_set[0:limit]) if limit > 0 else pprint.pprint(result_set)
