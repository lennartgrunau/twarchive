install:
	pip3 install --user --requirement requirements.txt

start:
	python3 run_webapp.py

fetch:
	python3 run_webapp.py

ustart:
	git pull && make start
