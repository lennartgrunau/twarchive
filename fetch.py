import csv
import getopt
import json
import os
import sys
from twython import Twython
import config

username = None

# Parse username, if one is given
(opts, args) = getopt.getopt(sys.argv[1:], "u:h", ["username=", "help"])
for (opt, arg) in opts:
    if opt in ("-u", "--username"):
        username = arg
    elif opt in ("-h", "--help"):
        print("""\nUsage:

    python fetch.py [-u <username>]

Parameters:
    -u (--username) <username>     The username of the account which should be fetched. Optional.
""")
        exit(0)

CSV_FILE_NAME = __file__[:-len(os.path.basename(__file__))] + (username.lower() if username else config.DEFAULT_ARCHIVE_NAME) + ".csv"
JSON_FILE_NAME = __file__[:-len(os.path.basename(__file__))] + "keys.json"


def fetch():
    """ Looks for new tweets.
        :returns: Amount of new tweets added to the archive. """

    # touch file and add csv header
    if not os.path.exists(CSV_FILE_NAME):
        f = open(CSV_FILE_NAME, "w", encoding="utf-8")
        f.write("id,created_at,source,text\n")
        f.close()

    tweet_file = csv.writer(open(CSV_FILE_NAME, "a", encoding="utf-8", newline=""))

    # open key file or exit if file does not exist
    try:
        key_file = open(JSON_FILE_NAME, "r")
    except FileNotFoundError:
        exit("[ERROR] Missing keys.json file.")

    keys = json.loads(key_file.read())

    # exit if keys are incomplete
    if len(keys) != 4:
        print("[ERROR] Invalid key file. Make sure that all 4 needed keys are included and seperated by line breaks.")
        exit(1)

    # check if each key is at least 20 chars long
    for _, value in keys.items():
        if len(value) < 20:
            exit("[ERROR] Your key file contains at least one invalid key. Please reconfigure it.")

    # map keys and create Twitter client
    CONSUMER_KEY = keys["CONSUMER_KEY"]
    CONSUMER_SECRET = keys["CONSUMER_SECRET"]
    ACCESS_KEY = keys["ACCESS_KEY"]
    ACCESS_SECRET = keys["ACCESS_SECRET"]

    # create Twitter API client
    twitter = Twython(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_KEY, ACCESS_SECRET)

    file = open(CSV_FILE_NAME, "r", encoding="utf-8")
    reader = csv.reader(file)
    lines = []
    for row in reader:
        lines.append(row)
    tail = lines[-1]
    offset = int(tail[0]) if tail[0] != "id" else 1
    file.close()

    # fetch the newest (up to 200) tweets that are newer than the newest
    # archived tweet
    result_set = twitter.get_user_timeline(count=config.TWEETS_PER_FETCH, since_id=offset, \
        include_rts=str(config.INCLUDE_RETWEETS).lower(), trim_user="false", tweet_mode="extended", \
        screen_name=(username if username else None))
    result_set = list(reversed(result_set)) # from oldest to newest
    tweets = []
    for tweet in result_set:
        new_tweet = []
        new_tweet.append(tweet["id"])
        new_tweet.append(tweet["created_at"])
        new_tweet.append(tweet["source"][tweet["source"].index(">") + 1:-4])
        content = tweet["full_text"]

        # Replace t.co shortlinks with full links
        urls = tweet["entities"]["urls"]
        for url in urls:
            content = content.replace(url["url"], url["expanded_url"])

        new_tweet.append(content)
        tweets.append(new_tweet)

    tweet_file.writerows(tweets)

    return len(tweets)

# Shell execution
if __name__ == "__main__":
    print(f"> {fetch()} new tweets fetched")
