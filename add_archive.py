import json
import csv
import getopt
import os
import sys
from datetime import datetime
import config

# Parse shell arguments.
# input: The path to the original Twitter archive JSON file ("tweet.js" in the archive)
# output: The path to the new archive file.
(in_archive, out_archive) = (None, None)

try:
    (opts, args) = getopt.getopt(sys.argv[1:], "i:o:h", ["input=", "output=", "help"])
except getopt.GetoptError as err:
    exit(err)

for (opt, arg) in opts:
    if opt in ("-i", "--input"):
        in_archive = arg
    elif opt in ("-o", "--output"):
        out_archive = arg
    elif opt in ("-h", "--help"):
        print(f"""\nUsage:

    python add_archive.py [-i tweet.js] [-o {config.DEFAULT_ARCHIVE_NAME}.csv]

Parameters:
    -i (--input) <filename>     The input file in the JS format by Twitter. Usally the tweet.js. Optional.
    -o (--output) <filename>    The output file. Default is {config.DEFAULT_ARCHIVE_NAME}.csv. Optional.
""")
        exit(0)

INPUT_FILE_NAME = __file__[:-len(os.path.basename(__file__))] + \
    (in_archive.lower() if in_archive else "tweet") + ".js"

OUTPUT_FILE_NAME = __file__[:-len(os.path.basename(__file__))] + (
    out_archive.lower() if out_archive else config.DEFAULT_ARCHIVE_NAME) + ".csv"

if not os.path.exists(OUTPUT_FILE_NAME):
    print("[INFO] The specified CSV output file doesn't exist. Creating new one.")
    f = open(OUTPUT_FILE_NAME, "w", encoding="utf-8")
    f.close()

# Check if input file is properly formatted
with open(INPUT_FILE_NAME) as f:
    lines = f.readlines()

if lines[0][0:6] == "window":
    # Replace first line
    lines[0] = "[{"

with open(INPUT_FILE_NAME, "w") as f:
    f.writelines(lines)

# Read tweets into CSV
tweets = []
with open(INPUT_FILE_NAME, "r", encoding="utf-8") as f:
    d = json.load(f)
    print("Number of tweets in the archive:", len(d))

    for tweet in d:

        # skip retweets
        if not config.INCLUDE_RETWEETS and tweet["full_text"][:4] == "RT @":
            continue

        new_tweet = []

        new_tweet.append(tweet["id_str"])
        new_tweet.append(tweet["created_at"])

        # Strip HTML tag
        new_tweet.append(tweet["source"][tweet["source"].index(">") + 1:-4])

        content = tweet["full_text"]

        # Replace t.co shortlinks with full links
        urls = tweet["entities"]["urls"]
        for url in urls:
            content = content.replace(url["url"], url["expanded_url"])

        new_tweet.append(content)
        tweets.append(new_tweet)

output_file = csv.writer(open(OUTPUT_FILE_NAME, "w", encoding="utf-8"))
output_file.writerow(["id", "created_at", "source", "text"])
output_file.writerows(list(reversed(tweets)))
